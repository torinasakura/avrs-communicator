/* eslint-disable no-console */
import createApp from '../src'

const app = createApp()

app.listen(3000, (error) => {
  if (error) {
    throw error
  }

  console.info('Server listening on port %s', 3000)
})
