Aversis System Communicator Repository
=============================

## Commands

Install dependencies

```
docker-compose run npm install
```

Start dev server

```
docker-compose up dev
```

Build image

```
docker-compose build server
```

Start production based server

```
docker-compose up server
```
