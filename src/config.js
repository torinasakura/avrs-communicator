import nconf from 'nconf'

nconf.argv().env({ separator: '_', lowerCase: true })

nconf.defaults({
  auth: {
    secret: 'Dev secret string',
  },
})

export default nconf
