import Express from 'express'
import { Server as server } from 'http'
import sio from 'socket.io'
import socketioJwt from 'socketio-jwt'
import config from './config'

export default function createApp(app = new Express()) {
  const http = server(app)
  const io = sio(http)

  const accounts = {}
  const events = ['status', 'stat', 'state']

  io.set('authorization', socketioJwt.authorize({
    secret: config.get('auth:secret'),
    handshake: true,
  }))

  io
    .on('connection', (socket) => {
      const { decoded_token, _query } = socket.conn.request

      socket.accountId = decoded_token.id // eslint-disable-line no-param-reassign
      socket.type = _query.type // eslint-disable-line no-param-reassign

      if (!accounts[socket.accountId]) {
        accounts[socket.accountId] = []
      }

      accounts[socket.accountId].push(socket.id)

      events.forEach((event) => {
        socket.on(event, (data) => {
          accounts[socket.accountId].forEach((id) => {
            const target = io.sockets.connected[id]

            if (target && socket.type !== target.type) {
              target.emit(event, data)
            }
          })
        })
      })

      socket.on('disconnect', () => {
        const connections = accounts[socket.accountId]

        if (connections) {
          connections.splice(connections.indexOf(socket.id), 1)
        }
      })
    })

  return http
}
